import unittest
import PIL.Image
from unittest.mock import patch
from convert import Conversion, Utility



class TestConvertToPdf(unittest.TestCase):
    
    def setUp(self):
        self.src = "logo.png"
        self.dest = "logo.pdf"
        

    @patch('PIL.Image.open')
    @patch('PIL.Image.Image.save')
    def test_convertToPdf(self, mock_save, mock_open):
    
        mock_open.return_value = PIL.Image.Image()
        mock_save.return_value = None

    
        Conversion.convertToPdf(self.src, self.dest)

        # Asserting the given values
        mock_open.assert_called_once_with(self.src)
        mock_save.assert_called_once_with(self.dest, "PDF", quality=100)

if __name__ == '__main__':

    unittest.main()