import argparse
import time
import PIL.Image
import traceback

class Utility:
    """

    Utility Class contains just static methods which
    will be reused across the whole script.
    """
    
    @staticmethod
    def get_current_time():
        """

        :return: type :: String
        """
        current_time = time.localtime()
        return time.strftime("%H:%M:%S", current_time)
    
    @staticmethod
    def save_log_file(data):
        with open("execution.log", 'a+') as file:
            file.write(data)
    
    @staticmethod
    def info_logger(text):
        """
        :param text: type :: String
        """
        print(Utility.get_current_time() + " - INFO - " + text)
        Utility.save_log_file("\n {} - INFO - {}".format(Utility.get_current_time(), text))

    @staticmethod
    def error_logger(text):
        """
        :param text: type :: String
        """
        print(Utility.get_current_time() + " - ERROR - " + text)
        Utility.save_log_file("\n {} - ERROR - {}".format(Utility.get_current_time(), text))
        exit(1)

    @staticmethod
    def get_arguments():
        try:
            parser = argparse.ArgumentParser()

            parser.add_argument('--input', action='store', dest='src',
                                help='Provide the input image file', required=True)
            parser.add_argument('--output', action='store',
                                dest='dest',
                                help='Provide the output filename', required=True)
            args = parser.parse_args()

            return args
        except Exception as error:
            Utility.error_logger("Failed to get the arguments due to : \n {} ".format(str(error)))

class Conversion: 
    """

    Conversion  Class contains business logic methods which
    will be executed after all the inputs are correctly provided.
    """
    @staticmethod
    def convertToPdf(src, dest): 

        try: 
            
            Utility.info_logger("Converting png file {}".format(src))
            im = PIL.Image.open(src)

            im.save(dest, "PDF", quality=100)
            Utility.info_logger("Converted to pdf file {}".format(dest))
        
        except Exception as error:
            Utility.error_logger(str(error) + " \n" + str(traceback.format_exc()))


if __name__ == '__main__':

    args = Utility.get_arguments()
    
    Conversion.convertToPdf(args.src, args.dest)





